package quickcheck

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.math.min

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    n <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(n, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("min1") = forAll { (n: Int) =>
    findMin(insert(n, empty)) == n
  }

  // don't worry if IntelliJ show errors in this function
  property("min3") = forAll { (h1: H, h2: H) =>
    (h1 != empty && h2 != empty) ==> (findMin(meld(h1, h2)) == min(findMin(h1), findMin(h2)))
  }

  property("min2") = forAll { (m: Int, n: Int) =>
    val mh = findMin(insert(m, insert(n, empty)))
    mh == min(m, n)
  }

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("del1") = forAll { (n: Int) =>
    empty == deleteMin(insert(n, empty))
  }

  property("meld1") = forAll { (h: H) =>
    (meld(empty, h) == h) && (meld(h, empty) == h)
  }

  def contains(h: H, x: Int): Boolean = {
    if (isEmpty(h)) false
    else if (x == findMin(h)) true
    else contains(deleteMin(h), x)
  }

  property("contains") = forAll { (h1: H, n: Int) =>
    contains(insert(n, h1), n)
  }

  def ordered(h: H): Boolean = {
    if (h == empty) true
    else {
      val h1 = deleteMin(h)
      if (h1 == empty) true
      else findMin(h) <= findMin(h1) && ordered(h1);
    }
  }

  property("ord1") = forAll { h: H =>
    ordered(h)
  }

  property("ord2") = forAll { (h1: H, h2: H) =>
    ordered(meld(h1, h2))
  }
}
